package edu.rit.cs;

import mpi.MPI;
import mpi.MPIException;


/**
 * Contains all the logic to parallelize the largest triangle computation
 * across several machines.
 */
public class LargeTriangleParallel {

    /**
     * Given the index of 3 separate points, determine the area of the triangle that
     * is defined by those points.
     * @param allPoints All of the points that have been generated.
     * @param submission The indices of the points for the triangle.
     * @return The triangle and its area
     */
    public static LargestTriangleSolution solutionFromSubmission(Point[] allPoints, int[] submission) {
        LargestTriangleSolution solution = new LargestTriangleSolution();
        solution.setPoint1Index(submission[0]);
        solution.setPoint1(allPoints[submission[0]]);

        solution.setPoint2Index(submission[1]);
        solution.setPoint2(allPoints[submission[1]]);

        solution.setPoint3Index(submission[2]);
        solution.setPoint3(allPoints[submission[2]]);

        solution.setTriangleArea(LargeTriangle.calculateTriangleArea(
            solution.getPoint1(),
            solution.getPoint2(),
            solution.getPoint3()
        ));

        return solution;
    }

    public static void main(String[] args) {
        try {
            if (args.length != 3) {
                System.out.println("Usage: LargeTriangleParallel point_count side seed");
                return;
            }
            MPI.Init(args);

            int pointCount = Integer.parseInt(args[0]); // 100
            int side = Integer.parseInt(args[1]); // 100
            int seed = Integer.parseInt(args[2]); // 142857

            int workerIndex = MPI.COMM_WORLD.getRank();
            int workerCount = MPI.COMM_WORLD.getSize();

            int workerChunkSize = pointCount / workerCount;
            int startPointIndex = workerIndex * workerChunkSize;
            int endPointIndex = (workerIndex + 1) * workerChunkSize;

            // generate all of the random points used
            //System.out.println("Generating all points...");
            Point[] allPoints = LargeTriangle.getAllPoints(pointCount, side, seed);
            //System.out.println("Done");

            // find the largest triangle solution for the range this worker is responsible for
            //System.out.printf("Finding largest triangle in range %d to %d...\n", startPointIndex, endPointIndex);
            LargestTriangleSolution solution = LargeTriangle.findLargestTriangle(allPoints, startPointIndex, endPointIndex);
            //System.out.println("Finished computing");

            if (workerIndex == 0) {
                //System.out.println("Collecting results...");

                // collect the results from all the workers
                for (int workerIndexCounter = 1; workerIndexCounter < workerCount; workerIndexCounter++) {
                    int[] submission = new int[3];
                    MPI.COMM_WORLD.recv(submission, submission.length, MPI.INT, workerIndexCounter, 0);

                    //System.out.printf("Got %d %d %d from %d\n", submission[0], submission[1], submission[2], workerIndexCounter);

                    LargestTriangleSolution incomingSolution = solutionFromSubmission(allPoints, submission);
                    if (incomingSolution.getTriangleArea() > solution.getTriangleArea()) {
                        solution = incomingSolution;
                    }
                }

                System.out.println(solution);
            } else {
                // submit result to the head worker
                int[] submission = { 
                    solution.getPoint1Index(),
                    solution.getPoint2Index(),
                    solution.getPoint3Index()
                };

                MPI.COMM_WORLD.send(submission, submission.length, MPI.INT, 0, 0);
                //System.out.printf("Sent result from %d to 0\n", workerIndex);
            }
            MPI.Finalize();
        } catch (MPIException ex) {
            //System.out.println("An exception occurred: " + ex.getMessage());
        }     
    }
}
