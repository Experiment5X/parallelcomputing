package edu.rit.cs;


/**
 * Contains the logic for finding the largest triangle
 * and code to run it sequentially.
 */
public class LargeTriangle
{
    /**
     * Calculate the area of a triangle defined by 3 points
     *
     * @param p1 First point
     * @param p2 Second point
     * @param p3 Third point
     * @return The area of the triangle
     */
    public static double calculateTriangleArea(Point p1, Point p2, Point p3) {
        double sideALength = p1.distanceTo(p2);
        double sideBLength = p2.distanceTo(p3);
        double sideCLength = p3.distanceTo(p1);

        double s = (sideALength + sideBLength + sideCLength) / 2;
        double area = Math.sqrt(s * (s - sideALength) * (s - sideBLength) * (s - sideCLength));

        return area;
    }

    /**
     * Find the largest triangle by area by looking at all possible triangles created by
     * a combination of 3 points in the array passed in. The first point in the triangle
     * will only be searched in the range p1Start to p1End, but the other vertices in the
     * triangle can be at any position in the array. This split is done for parallelization.
     *
     * @param points The points to check
     * @param p1Start The start index for the first vertex
     * @param p1End The end index for the first vertex
     * @return A triangle with the largest area.
     */
    public static LargestTriangleSolution findLargestTriangle(Point[] points, int p1Start, int p1End) {
        LargestTriangleSolution solution = null;
        for (int p1Index = p1Start; p1Index < p1End; p1Index++) {
            Point p1 = points[p1Index];
            for (int p2Index = 0; p2Index < points.length; p2Index++) {
                Point p2 = points[p2Index];
                for (int p3Index = 0; p3Index < points.length; p3Index++) {
                    Point p3 = points[p3Index];

                    double area = calculateTriangleArea(p1, p2, p3);
                    if (solution == null || area > solution.getTriangleArea()) {
                        solution = new LargestTriangleSolution();

                        solution.setPoint1Index(p1Index);
                        solution.setPoint1(p1);

                        solution.setPoint2Index(p2Index);
                        solution.setPoint2(p2);

                        solution.setPoint3Index(p3Index);
                        solution.setPoint3(p3);

                        solution.setTriangleArea(area);
                    }
                }
            }
        }

        return solution;
    }

    /**
     * Generate all the random points with a given seed
     *
     * @param numPoints The total number of points to generate
     * @param side The max (x, y) value for the points
     * @param seed The seed to seed the RNG with
     * @return An array of all the points requested
     */
    public static Point[] getAllPoints(int numPoints, int side, long seed) {
        RandomPoints rndPoints = new RandomPoints(numPoints, side, seed);
        Point[] points = new Point[rndPoints.size()];

        for (int i = 0; rndPoints.hasNext(); i++) {
            points[i] = rndPoints.next();
        }

        return points;
    }

    public static void main( String[] args ) {
        if (args.length != 3) {
            System.out.println("Usage: LargeTriangle point_count side seed");
            return;
        }
        int pointCount = Integer.parseInt(args[0]); // 100
        int side = Integer.parseInt(args[1]); // 100
        int seed = Integer.parseInt(args[2]); // 142857

        Point[] allPoints = getAllPoints(pointCount, side, seed);

        LargestTriangleSolution solution = findLargestTriangle(allPoints, 0, allPoints.length);
        System.out.println(solution);
    }
}
