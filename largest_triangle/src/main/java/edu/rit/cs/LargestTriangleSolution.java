package edu.rit.cs;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;


/**
 * Class representing a triangle that includes the points that make up the
 * triangle, and the indices of those points.
 */
public class LargestTriangleSolution {
    private int point1Index;
    private Point point1;

    private int point2Index;
    private Point point2;

    private int point3Index;
    private Point point3;

    private double triangleArea;

    class PointIndexPair implements Comparable<PointIndexPair> {
        public int index;
        public Point point;

        public PointIndexPair(int index, Point point) {
            this.index = index;
            this.point = point;
        }

        public int compareTo(PointIndexPair other) {
            return this.index - other.index;
        }
    }

    /**
     * Convert this solution into a string output where the points are sorted by their
     * index.
     *
     * @return A string representation of this object.
     */
    @Override
    public String toString() {

        List<PointIndexPair> points = new ArrayList<>();
        points.add(new PointIndexPair(point1Index, point1));
        points.add(new PointIndexPair(point2Index, point2));
        points.add(new PointIndexPair(point3Index, point3));

        Collections.sort(points);

        String output = "";
        for (PointIndexPair pair : points) {
            output += String.format("%d %.5g %.5g\n", (pair.index + 1), 
                        pair.point.getX(), pair.point.getY());
        }
        output += String.format("%.5g\n", triangleArea);

        return output;
    }

    public int getPoint1Index() {
        return point1Index;
    }

    public void setPoint1Index(int point1Index) {
        this.point1Index = point1Index;
    }

    public Point getPoint1() {
        return point1;
    }

    public void setPoint1(Point point1) {
        this.point1 = point1;
    }

    public int getPoint2Index() {
        return point2Index;
    }

    public void setPoint2Index(int point2Index) {
        this.point2Index = point2Index;
    }

    public Point getPoint2() {
        return point2;
    }

    public void setPoint2(Point point2) {
        this.point2 = point2;
    }

    public int getPoint3Index() {
        return point3Index;
    }

    public void setPoint3Index(int point3Index) {
        this.point3Index = point3Index;
    }

    public Point getPoint3() {
        return point3;
    }

    public void setPoint3(Point point3) {
        this.point3 = point3;
    }

    public double getTriangleArea() {
        return triangleArea;
    }

    public void setTriangleArea(double triangleArea) {
        this.triangleArea = triangleArea;
    }

}
