<h2>Largest Triangle</h2>
<p><a name="system"></a></p>
<p>Given a group of two-dimensional points, we want to find the&nbsp;<strong>largest triangle,</strong>&nbsp;namely three distinct points that are the vertices of a triangle with the largest area. The area of a triangle is (<em>s</em>(<em>s</em>&nbsp;&minus;&nbsp;<em>a</em>)(<em>s</em>&nbsp;&minus;&nbsp;<em>b</em>)(<em>s</em>&nbsp;&minus;&nbsp;<em>c</em>))<sup>1/2</sup>, where&nbsp;<em>a</em>,&nbsp;<em>b</em>, and&nbsp;<em>c</em>&nbsp;are the lengths of the triangle's sides and&nbsp;<em>s</em>&nbsp;=&nbsp;(<em>a</em>&nbsp;+&nbsp;<em>b</em>&nbsp;+&nbsp;<em>c</em>)/2. The length of a triangle's side is the Euclidean distance between the side's two endpoints.</p>
<p>You will write sequential and cluster parallel versions of a program that finds the largest triangle in a group of points. The program's command line argument is a&nbsp;<em>constructor expression</em>&nbsp;for a PointSpec object from which the program obtains the points' (<em>x,y</em>) coordinates. </p>

<h4>My output</h4>
<code>
axs9701 ~/Developer/ParallelSystems/parallelcomputing/largest_triangle/src/main/java $ time java  edu.rit.cs.LargeTriangle 100 100 14285
15 96.316 94.046
19 0.90955 90.717
58 35.939 0.00072836
4385.8


real	0m0.579s
user	0m0.233s
sys	0m0.052s
axs9701 ~/Developer/ParallelSystems/parallelcomputing/largest_triangle/src/main/java $ time mpirun --hostfile ~/Developer/ParallelSystems/parallelcomputing/largest_triangle/hosts --prefix /usr/local  java -cp  ~/Developer/ParallelSystems/mpi.jar edu.rit.cs.LargeTriangleParallel 100 100 14285
15 96.316 94.046
19 0.90955 90.717
58 35.939 0.00072836
4385.8


real	0m3.320s
user	0m0.826s
sys	0m0.432s
axs9701 ~/Developer/ParallelSystems/parallelcomputing/largest_triangle/src/main/java $ time mpirun --hostfile ~/Developer/ParallelSystems/parallelcomputing/largest_triangle/hosts --prefix /usr/local  java -cp  ~/Developer/ParallelSystems/mpi.jar edu.rit.cs.LargeTriangleParallel 1000 1000 14285
174 7.9055 907.48
655 968.15 5.0822
993 993.71 997.93
4.8822e+05


real	0m5.038s
user	0m6.132s
sys	0m0.432s
axs9701 ~/Developer/ParallelSystems/parallelcomputing/largest_triangle/src/main/java $ time java  edu.rit.cs.LargeTriangle 1000 1000 14285
174 7.9055 907.48
655 968.15 5.0822
993 993.71 997.93
4.8822e+05


real	0m8.066s
user	0m7.644s
sys	0m0.077s
</code>

<h4>Running the Application</h4>
<p>Build the docker container with my source code in it:</p>
<code>
docker build -t csci654:latest .
</code>
<p>Start the docker containers</p>
<code>
cd ../docker
</code>
<br />
<code>
bash createContainers.bash 3
</code>
<p>
Connect to each of the docker containers
</p>
<code>
docker exec -it CONTAINER_ID bash
</code>
<p>
Initialize the container
</p>
<code>
git pull; cd /csci654/docker/; bash initContainer.bash
</code>
<p>
Compile my code
</p>
<code>
cd /csci654/largest_triangle
</code>
<br />
<code>
mpijavac -cp /csci654Tools/mpi.jar ./main/java/edu/rit/cs/*.java
</code>
<p>
Edit the hosts file to contain the IPs of all the other docker containers. Then, run my application.
</p>
<code>
cd ./main/java
</code>
<br />
<code>
mpiexec --hostfile /csci654/largest_triangle/hosts --allow-run-as-root java -cp /csci654Tools/mpi.jar  edu.rit.cs.LargeTriangleParallel 100 100 142857
</code>
<p>To run it sequentially, in the same directory run</p>
<code>
java  edu.rit.cs.LargeTriangle 1000 1000 123
</code>